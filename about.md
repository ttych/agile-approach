---
layout: page
title: About
permalink: /about/
---

This is a personal compilation of agile related topics.

You can contribute through :
- comments
- github PR, Issues, ...

For all contents, I try to add all the references I used.
If some are missing, wrong, or not up-to-date don't hesitate to contact me.


Technically, it is based on [Jekyll](https://jekyllrb.com/),
and is hosted by [github](https://github.com/).
