---
title:  "Welcome to Agile Approach!"
date:   2019-03-23 10:35:11 +0100
categories: news
---

Finally, here it is !

I wanted to start this website a long time ago, but never had time to work on
it.

My plan is to put all my references, workshop, thoughts on agile topics.
I have a lot of notes and I want to put everything in one location. Share.
Have feedback, comments, ...
And then make it grow.

I will first put some contents and then work on the layout, so it will be
structured as documentation website.

The [Agile Approache website][website] is hosted on [github][github].

The [Repository][repository] is also on [github][github].


See you soon.

[github]: https://github.com/
[website]: https://ttych.github.io/agile-approach/
[repository]: https://github.com/ttych/agile-approach
